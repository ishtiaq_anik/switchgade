<?php

namespace App\Http\Controllers;

class PublicController extends Controller {

    public function home() {

        return view('home')->withNav('home');
    }

    public function productList() {
        return view('product-list')->withNav('product-list');
    }
    public function productDetails() {
        return view('product-details')->withNav('product-details');
    }
    public function productCategories() {
        return view('product-categories')->withNav('product-categories');
    }
    public function userDashboard() {
        return view('users.user-dashboard')->withNav('user-dashboard');
    }

    public function policy() {
       return view('privacy-policy');
    }



}
