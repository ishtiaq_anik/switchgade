@extends('layouts.master')

@section('title', 'Product Details')

@section('content')
<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->

<section>
    <div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordegreen1 c-bordegreen1-both">
        <div class="container">
            <div class="c-page-title c-pull-left">
                <h3 class="c-font-uppercase c-font-sbold">Product Details 2</h3>
                <h4 class="">Page Sub Title Goes Here</h4>
            </div>
            <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
                <li><a href="shop-product-details-2.html">Product Details 2</a></li>
                <li>/</li>
                <li class="c-state_active">Jango Components</li>

            </ul>
        </div>
    </div>

</section>

<section>


    <div class="c-content-box c-size-lg c-overflow-hide c-bg-white">
        <div class="container">
            <div class="c-shop-product-details-2">
                <div class="row">
                    <div class="col-md-6">
                        <div class="c-product-gallery">
                            <div class="c-product-gallery-content">
                                <div class="c-zoom" img_order="1" style="position: relative; overflow: hidden;">
                                    <img src="assets/img/content/shop3/79.jpg">
                                    <img src="assets/img/content/shop3/79.jpg" class="zoomImg" style="position: absolute; top: -154px; left: -29.3919px; opacity: 0; width: 700px; height: 900px; border: none; max-width: none; max-height: none;"></div>
                                    <div class="c-zoom c-hide" img_order="2" style="position: relative; overflow: hidden;">
                                        <img src="assets/img/content/shop3/82.jpg">
                                        <img src="assets/img/content/shop3/82.jpg" class="zoomImg" style="position: absolute; top: 0px; left: 0px; opacity: 0; width: 700px; height: 900px; border: none; max-width: none; max-height: none;"></div>
                                        <div class="c-zoom c-hide" img_order="3" style="position: relative; overflow: hidden;">
                                            <img src="assets/img/content/shop3/85.jpg">
                                            <img src="assets/img/content/shop3/85.jpg" class="zoomImg" style="position: absolute; top: 0px; left: 0px; opacity: 0; width: 700px; height: 900px; border: none; max-width: none; max-height: none;"></div>
                                            <div class="c-zoom c-hide" img_order="4" style="position: relative; overflow: hidden;">
                                                <img src="assets/img/content/shop3/90.jpg">
                                                <img src="assets/img/content/shop3/90.jpg" class="zoomImg" style="position: absolute; top: 0px; left: 0px; opacity: 0; width: 700px; height: 900px; border: none; max-width: none; max-height: none;"></div>
                                            </div>
                                            <div class="row c-product-gallery-thumbnail">
                                                <div class="col-xs-3 c-product-thumb" style="height: 136px;">
                                                    <img src="assets/img/content/shop3/79.jpg" img_order="1">
                                                </div>
                                                <div class="col-xs-3 c-product-thumb" style="height: 136px;">
                                                    <img src="assets/img/content/shop3/82.jpg" img_order="2">
                                                </div>
                                                <div class="col-xs-3 c-product-thumb" style="height: 136px;">
                                                    <img src="assets/img/content/shop3/85.jpg" img_order="3">
                                                </div>
                                                <div class="col-xs-3 c-product-thumb c-product-thumb-last" style="height: 136px;">
                                                    <img src="assets/img/content/shop3/90.jpg" img_order="4">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="c-product-meta">
                                            <div class="c-content-title-1">
                                                <h3 class="c-font-uppercase c-font-bold">iPhone 32 GB</h3>
                                                <div class="c-line-left"></div>
                                            </div>

                                            <div class="c-product-review">
                                                <div class="c-product-rating">
                                                    <i class="fa fa-star c-font-green1"></i>
                                                    <i class="fa fa-star c-font-green1"></i>
                                                    <i class="fa fa-star c-font-green1"></i>
                                                    <i class="fa fa-star c-font-green1"></i>
                                                    <i class="fa fa-star-half-o c-font-green1"></i>
                                                </div>
                                                <div class="c-product-write-review">
                                                    <a class="c-font-green1" href="#">Write a review</a>
                                                </div>
                                            </div>
                                            <div class="c-product-price">$99.00</div>
                                            <div class="c-product-short-desc">
                                                Lorem ipsum dolor ut sit ame dolore adipiscing elit, sed nonumy nibh sed euismod laoreet dolore magna aliquarm erat volutpat Nostrud duis molestie at dolore.
                                            </div>
                                            <div class="row c-product-variant">
                                                <div class="col-sm-12 col-xs-12">
                                                    <p class="c-product-meta-label c-product-margin-1 c-font-uppercase c-font-bold">Size:</p>
                                                    <div class="c-product-size">
                                                        <select>
                                                            <option value="S">S</option>
                                                            <option value="M">M</option>
                                                            <option value="L">L</option>
                                                            <option value="XL">XL</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-xs-12 c-margin-t-20">
                                                    <div class="c-product-color">
                                                        <p class="c-product-meta-label c-font-uppercase c-font-bold">Color:</p>
                                                        <select>
                                                            <option value="green1">green1</option>
                                                            <option value="Black">Black</option>
                                                            <option value="Beige">Beige</option>
                                                            <option value="White">White</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="c-product-add-cart c-margin-t-20">
                                                <div class="row">
                                                    <div class="col-sm-4 col-xs-12">
                                                        <div class="c-input-group c-spinner">
                                                            <p class="c-product-meta-label c-product-margin-2 c-font-uppercase c-font-bold">QTY:</p>
                                                            <input type="text" class="form-control c-item-1" value="1">
                                                            <div class="c-input-group-btn-vertical">
                                                                <button class="btn btn-default" type="button" data_input="c-item-1"><i class="fa fa-caret-up"></i></button>
                                                                <button class="btn btn-default" type="button" data_input="c-item-1"><i class="fa fa-caret-down"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-xs-12 c-margin-t-20">
                                                        <button class="btn c-btn btn-lg c-font-bold c-font-white c-theme-btn c-btn-square c-font-uppercase">Add to Cart</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="c-content-box c-size-md c-no-padding">
                        <div class="c-shop-product-tab-1" role="tabpanel">
                            <div class="container">
                                <ul class="nav nav-justified" role="tablist">
                                    <li role="presentation" class="active">
                                        <a class="c-font-uppercase c-font-bold" href="#tab-1" role="tab" data-toggle="tab">Description</a>
                                    </li>
                                    <li role="presentation">
                                        <a class="c-font-uppercase c-font-bold" href="#tab-2" role="tab" data-toggle="tab">Additional Information</a>
                                    </li>
                                    <li role="presentation">
                                        <a class="c-font-uppercase c-font-bold" href="#tab-3" role="tab" data-toggle="tab">Reviews (3)</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="tab-1">
                                    <div class="c-product-desc c-center">
                                        <div class="container">

                                            <p>
                                                Lorem ipsum dolor sit amet, consectetuer
                                                adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                                magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud
                                                exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
                                                consequat.
                                            </p>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetuer
                                                adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                                magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud
                                                exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
                                                consequat. Lorem ipsum dolor sit amet, consectetuer
                                                adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                                magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud
                                                exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
                                                consequat.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="tab-2">
                                    <div class="container">
                                        <p class="c-center"><strong>Sizes:</strong> S, M, L, XL</p><br>
                                        <p class="c-center"><strong>Colors:</strong> green1, Black, Beige, White</p><br>
                                    </div>
                                    <div class="c-product-tab-meta-bg c-bg-grey c-center">
                                        <div class="container">
                                            <p class="c-product-tab-meta"><strong>SKU:</strong> 1410SL</p>
                                            <p class="c-product-tab-meta"><strong>Categories:</strong> <a href="#">Jacket</a>, <a href="#">Winter</a></p>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab-3">
                                    <div class="container">
                                        <h3 class="c-font-uppercase c-font-bold c-font-22 c-center c-margin-b-40 c-margin-t-40">Reviews for Warm Winter Jacket</h3>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="c-user-avatar">
                                                    <img src="assets/img/content/avatars/team1.jpg">
                                                </div>
                                                <div class="c-product-review-name">
                                                    <h3 class="c-font-bold c-font-24 c-margin-b-5">Steve</h3>
                                                    <p class="c-date c-theme-font c-font-14">July 4, 2015</p>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="c-product-rating c-right">
                                                    <i class="fa fa-star c-font-green1"></i>
                                                    <i class="fa fa-star c-font-green1"></i>
                                                    <i class="fa fa-star c-font-green1"></i>
                                                    <i class="fa fa-star c-font-green1"></i>
                                                    <i class="fa fa-star-half-o c-font-green1"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="c-product-review-content">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetuer
                                                adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                                magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud
                                            </p>
                                        </div>
                                        <div class="row c-margin-t-40">
                                            <div class="col-xs-6">
                                                <div class="c-user-avatar">
                                                    <img src="assets/img/content/avatars/team12.jpg">
                                                </div>
                                                <div class="c-product-review-name">
                                                    <h3 class="c-font-bold c-font-24 c-margin-b-5">John</h3>
                                                    <p class="c-date c-theme-font c-font-14">July 4, 2015</p>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="c-product-rating c-right">
                                                    <i class="fa fa-star c-font-green1"></i>
                                                    <i class="fa fa-star c-font-green1"></i>
                                                    <i class="fa fa-star c-font-green1"></i>
                                                    <i class="fa fa-star c-font-green1"></i>
                                                    <i class="fa fa-star-half-o c-font-green1"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="c-product-review-content">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetuer
                                                adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                                magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud
                                            </p>
                                        </div>
                                        <div class="row c-margin-t-40">
                                            <div class="col-xs-6">
                                                <div class="c-user-avatar">
                                                    <img src="assets/img/content/avatars/team8.jpg">
                                                </div>
                                                <div class="c-product-review-name">
                                                    <h3 class="c-font-bold c-font-24 c-margin-b-5">Alice</h3>
                                                    <p class="c-date c-theme-font c-font-14">July 4, 2015</p>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="c-product-rating c-right">
                                                    <i class="fa fa-star c-font-green1"></i>
                                                    <i class="fa fa-star c-font-green1"></i>
                                                    <i class="fa fa-star c-font-green1"></i>
                                                    <i class="fa fa-star c-font-green1"></i>
                                                    <i class="fa fa-star-half-o c-font-green1"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="c-product-review-content">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetuer
                                                adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                                magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud
                                            </p>
                                        </div>
                                        <div class="row c-product-review-input">
                                            <h3 class="c-font-bold c-font-uppercase">Submit your Review</h3>
                                            <p class="c-review-rating-input">Rating:
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </p>
                                            <textarea></textarea>
                                            <button class="btn c-btn c-btn-square c-theme-btn c-font-bold c-font-uppercase c-font-white">Submit Review</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- BEGIN: CONTENT/SHOPS/SHOP-2-2 -->


                @endsection


                @section('page_script_three')

                <!-- BEGIN: PAGE SCRIPTS -->
                <script src="{{ asset('assets/plugins/zoom-master/jquery.zoom.min.js') }}" type="text/javascript"></script>
                <!-- END: PAGE SCRIPTS -->
                <!-- END: LAYOUT/BASE/BOTTOM -->

                @endsection
