<a name="footer"></a>
<footer class="c-layout-footer c-layout-footer-6 c-bg-grey-1">
    <div class="container">
        <div class="c-prefooter c-bg-white">

            <div class="c-body">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="c-content-title-1 c-title-md">
                            <h3 class="c-title c-font-uppercase c-font-bold">Buy & Sell</h3>
                            <div class="c-line-left hide"></div>
                        </div>
                        <ul class="c-links c-theme-ul">
                            <li>
                                <a href="#">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="#">Terms & Conditions</a>
                            </li>
                            <li>
                                <a href="#">Delivery</a>
                            </li>
                            <li>
                                <a href="#">Promotions</a>
                            </li>
                            <li>
                                <a href="#">News</a>
                            </li>
                        </ul>

                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="c-content-title-1 c-title-md">
                            <h3 class="c-title c-font-uppercase c-font-bold">About SwitchGads</h3>
                            <div class="c-line-left hide"></div>
                        </div>
                        <ul class="c-links c-theme-ul">

                            <li>
                                <a href="#">Projects</a>
                            </li>
                            <li>
                                <a href="#">Clients</a>
                            </li>
                            <li>
                                <a href="#">Services</a>
                            </li>
                            <li>
                                <a href="#">Features</a>
                            </li>
                            <li>
                                <a href="#">Stats</a>
                            </li>
                        </ul>

                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="c-content-title-1 c-title-md">
                            <h3 class="c-title c-font-uppercase c-font-bold">Account</h3>
                            <div class="c-line-left hide"></div>
                        </div>
                        <ul class="c-links c-theme-ul">

                            <li>
                                <a href="#">Projects</a>
                            </li>
                            <li>
                                <a href="#">Clients</a>
                            </li>
                            <li>
                                <a href="#">Services</a>
                            </li>
                            <li>
                                <a href="#">Features</a>
                            </li>
                            <li>
                                <a href="#">Stats</a>
                            </li>
                        </ul>

                    </div>

                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="c-content-title-1 c-title-md">
                            <h3 class="c-title c-font-uppercase c-font-bold">Contact Us</h3>
                            <div class="c-line-left hide"></div>
                        </div>
                        <p class="c-address c-font-16"> 25, Lorem Lis Street, Orange
                            <br/> California, US
                            <br/> Phone: 800 123 3456
                            <br/> Fax: 800 123 3456
                            <br/> Email:
                            <a href="mailto:info@switchgads.com">
                                <span class="c-theme-color">info@switchgads.com</span>
                            </a>
                            <br/> Skype:
                            <a href="#">
                                <span class="c-theme-color">switchgads</span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="c-line"></div>
            <div class="c-foot">
                <div class="row">
                    <div class="col-md-7">
                        <div class="c-content-title-1 c-title-md">
                            <h3 class="c-font-uppercase c-font-bold">About
                                <span class="c-theme-font">switchgads</span>
                            </h3>
                            <div class="c-line-left hide"></div>
                        </div>
                        <p class="c-text c-font-16 c-font-regular">Tolerare unus ducunt ad brevis buxum. Est alter buxum, cesaris. Eheu, lura! Racanas crescere in emeritis oenipons! Ubi est rusticus repressor? Lixa grandis clabulare est. Eposs tolerare.</p>
                    </div>
                    <div class="col-md-5">
                        <div class="c-content-title-1 c-title-md">
                            <h3 class="c-font-uppercase c-font-bold">Subscribe to Newsletter</h3>
                            <div class="c-line-left hide"></div>
                        </div>
                        <div class="c-line-left hide"></div>
                        <form action="#">
                            <div class="input-group input-group-lg c-square">
                                <input type="text" class="form-control c-square c-font-grey-3 c-border-grey c-theme" placeholder="Your Email Here" />
                                <span class="input-group-btn">
                                    <button class="btn c-theme-btn c-theme-border c-btn-square c-btn-uppercase c-font-16" type="button">Subscribe</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="c-postfooter c-bg-dark-2">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 c-col">
                    <p class="c-copyright c-font-grey">2015 &copy; SwitchGads
                        <span class="c-font-grey-3">All Rights Reserved.</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- END: LAYOUT/FOOTERS/FOOTER-6 -->
<!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
<div class="c-layout-go2top">
    <i class="icon-arrow-up"></i>
</div>
<!-- END: LAYOUT/FOOTERS/GO2TOP -->
