@extends('layouts.master')
@section('title', 'Home')
@section('content')
<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
<section>
    <div class="c-content-box c-size-sm c-bg-white">
        <div class="container">
            <form class="c-shop-advanced-search-1">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control c-square c-theme input-lg" placeholder="Enter keywords or item number">
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold"><i class="fa fa-search"></i>Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>	<!-- BEGIN: PAGE CONTENT -->
<section>
    <div class="c-content-box c-size-sm c-bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 wow animate fadeInLeft">
                    <div class="c-content-media-2-slider" data-slider="owl">
                        <div class="c-content-label c-font-uppercase c-font-bold hide">Latest Uploads</div>
                        <div class="owl-carousel owl-theme c-theme owl-single" data-single-item="true" data-auto-play="4000" data-rtl="false">
                            <div class="item">
                                <div class="c-content-media-2 c-bg-img-center" style="background-image: url(assets/img/content/slider/slider.jpg); min-height: 360px;">
                                    <div class="c-panel">
                                        <div class="c-fav">
                                            <i class="icon-heart c-font-thin"></i>
                                            <p class="c-font-thin">16</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="c-content-media-2 c-bg-img-center" style="background-image: url(assets/img/content/slider/slider.jpg); min-height: 360px;">
                                    <div class="c-panel">
                                        <div class="c-fav">
                                            <i class="icon-heart c-font-thin"></i>
                                            <p class="c-font-thin">24</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="c-content-media-2 c-bg-img-center" style="background-image: url(assets/img/content/slider/slider.jpg); min-height: 360px;">
                                    <div class="c-panel">
                                        <div class="c-fav">
                                            <i class="icon-heart c-font-thin"></i>
                                            <p class="c-font-thin">19</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 c-padding-20 c-bg-white c-bg-img-center-contain" style="background-image:url(assets/img/content/misc/s-bg.jpg)">
                    <div class="c-content-title-1 c-margin-t-20">
                        <h3 class=" c-font-bold c-font-default c-font-green1">Why<br> SwitchGads</h3>
                        <div class="c-line-left c-font-green1"></div>

                        <p class="c-font-thin">Ask your questions away and let our dedicated customer service help you look through our FAQs to get your questions answered!</p>
                        <p><a href="#" class="btn c-btn-dark c-btn-square">Learn More</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="c-content-box c-size-sm c-bg-white  ">
        <div class="container">
            <div class="c-content-feature-2-grid" data-auto-height="true" data-mode="base-height">

                <div class="c-content-title-1">
                    <h3 class="c-left c-font-lowercase c-font-bold">Services We Do</h3>
                    <div class="c-line-left c-theme-bg"></div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="c-content-feature-2" data-height="height" style="height: 160px;">
                            <div class="c-icon-wrapper">
                                <div class="c-content-line-icon c-theme c-icon-8"></div>
                            </div>
                            <h3 class="c-font-uppercase c-font-bold c-font-17 c-font-green1">Buy With confidence</h3>
                            <p>Lorem ipsum consectetuer dolore elit diam</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="c-content-feature-2" data-height="height" style="height: 160px;">
                            <div class="c-icon-wrapper">
                                <div class="c-content-line-icon c-theme c-icon-30"></div>
                            </div>
                            <h3 class="c-font-uppercase c-font-bold c-font-17 c-font-green1">No Selling Fees! $0!</h3>
                            <p>Lorem ipsum consectetuer dolore elit diam</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="c-content-feature-2" data-height="height" style="height: 160px;">
                            <div class="c-icon-wrapper">
                                <div class="c-content-line-icon c-theme c-icon-32"></div>
                            </div>
                            <h3 class="c-font-uppercase c-font-bold  c-font-17 c-font-green1">Safer then meeting up</h3>
                            <p>Lorem ipsum consectetuer dolore elit diam</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <!-- BEGIN: CONTENT/SHOPS/SHOP-2-2 -->
    <div class="c-content-box c-size-sm c-overflow-hide c-bs-grid-small-space">
        <div class="container">
            <div class="c-content-title-1">
                <h3 class="c-left c-font-lowercase c-font-bold">Take Your Pick!</h3>
                <div class="c-line-left c-theme-bg"></div>
            </div>
            <div class="row">
                <div data-slider="owl">
                    <div class="owl-carousel owl-theme c-theme owl-small-space c-owl-nav-center" data-rtl="false" data-items="4" data-slide-speed="8000">
                        <div class="item">
                            <div class="c-content-product-2 c-bg-white c-border">
                                <div class="c-content-overlay">

                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                        </div>
                                    </div>
                                    <div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/img/content/shop5/25.png);"></div>
                                </div>
                                <div class="c-info">
                                    <p class="c-title c-font-18 c-font-slim c-center">iPhones</p>
                                    <p class="c-price c-font-16 c-font-slim c-center ">$300 and up &nbsp;
                                    </p>
                                </div>
                                <div class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group c-border-top" role="group">
                                        <a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Details</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="c-content-product-2 c-bg-white c-border">
                                <div class="c-content-overlay">

                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                        </div>
                                    </div>
                                    <div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/img/content/shop5/18.png);"></div>
                                </div>
                                <div class="c-info">
                                    <p class="c-title c-font-18 c-font-slim c-center">Smartphones</p>
                                    <p class="c-price c-font-16 c-font-slim c-center ">$300 and up &nbsp;
                                    </p>
                                </div>
                                <div class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group c-border-top" role="group">
                                        <a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Details</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="c-content-product-2 c-bg-white c-border">
                                <div class="c-content-overlay">

                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                        </div>
                                    </div>
                                    <div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/img/content/shop5/32.png);"></div>
                                </div>
                                <div class="c-info">
                                    <p class="c-title c-font-18 c-font-slim c-center">Watches</p>
                                    <p class="c-price c-font-16 c-font-slim c-center ">$300 and up &nbsp;
                                    </p>
                                </div>
                                <div class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group c-border-top" role="group">
                                        <a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Details</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="c-content-product-2 c-bg-white c-border">
                                <div class="c-content-overlay">

                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                        </div>
                                    </div>
                                    <div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/img/content/shop5/29.png);"></div>
                                </div>
                                <div class="c-info">
                                    <p class="c-title c-font-18 c-font-slim c-center">Tablets</p>
                                    <p class="c-price c-font-16 c-font-slim c-center ">$300 and up &nbsp;
                                    </p>
                                </div>
                                <div class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group c-border-top" role="group">
                                        <a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Details</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="c-content-product-2 c-bg-white c-border">
                                <div class="c-content-overlay">

                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                        </div>
                                    </div>
                                    <div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/img/content/shop5/08.png);"></div>
                                </div>
                                <div class="c-info">
                                    <p class="c-title c-font-18 c-font-slim c-center">Macbooks</p>
                                    <p class="c-price c-font-16 c-font-slim c-center ">$300 and up &nbsp;
                                    </p>
                                </div>
                                <div class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group c-border-top" role="group">
                                        <a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Details</a>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div><!-- END: CONTENT/SHOPS/SHOP-2-2 -->

</section>
@endsection
