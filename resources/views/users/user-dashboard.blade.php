@extends('layouts.master')

@section('title', 'Product Details')

@section('content')
<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->

<section>
    <div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordegreen1 c-bordegreen1-both">
        <div class="container">
            <div class="c-page-title c-pull-left">
                <h3 class="c-font-uppercase c-font-sbold">Product Details 2</h3>
                <h4 class="">Page Sub Title Goes Here</h4>
            </div>
            <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
                <li><a href="shop-product-details-2.html">Product Details 2</a></li>
                <li>/</li>
                <li class="c-state_active">SwitchGads Components</li>

            </ul>
        </div>
    </div>

</section>
<div class="container">
<section>

        <div class="c-layout-sidebar-menu c-theme ">
            <!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
            <div class="c-sidebar-menu-toggler">
                <h3 class="c-title c-font-uppercase c-font-bold">My Profile</h3>
                <a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
                    <span class="c-line"></span> <span class="c-line"></span> <span class="c-line"></span>
                </a>
            </div>

            <ul class="c-sidebar-menu collapse " id="sidebar-menu-1">
                <li class="c-dropdown c-open">
                    <a href="javascript:;" class="c-toggler">My Profile<span class="c-arrow"></span></a>
                    <ul class="c-dropdown-menu">
                        <li class="c-active">
                            <a href="shop-customer-dashboard.html">My Dashbord</a>
                        </li>
                        <li class="">
                            <a href="shop-customer-profile.html">Edit Profile</a>
                        </li>
                        <li class="">
                            <a href="shop-order-history.html">Order History</a>
                        </li>
                        <li class="">
                            <a href="shop-customer-addresses.html">My Addresses</a>
                        </li>
                        <li class="">
                            <a href="shop-product-wishlist.html">My Wishlist</a>
                        </li>
                    </ul>
                </li>
            </ul><!-- END: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
        </div>

</section>
<section>
    <div class="c-layout-sidebar-content ">
        <!-- BEGIN: PAGE CONTENT -->
        <!-- BEGIN: CONTENT/SHOPS/SHOP-CUSTOMER-DASHBOARD-1 -->
        <div class="c-content-title-1">
            <h3 class="c-font-uppercase c-font-bold">My Dashboard</h3>
            <div class="c-line-left"></div>
            <p class="">
                Hello <a href="#" class="c-theme-link">Drake Hiro</a> (not <a href="#" class="c-theme-link">Drake Hiro</a>? <a href="#" class="c-theme-link">Sign out</a>). <br>
            </p>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 c-margin-b-20">
                <h3 class="c-font-uppercase c-font-bold">Drake Hiro</h3>
                <ul class="list-unstyled">
                    <li>25, Lorem Lis Street, Orange C, California, US</li>
                    <li>Phone: 800 123 3456</li>
                    <li>Fax: 800 123 3456</li>
                    <li>Email: <a href="mailto:switchgads@themehats.com" class="c-theme-link">switchgads@themehats.com</a></li>
                </ul>
            </div>
        </div><!-- END: CONTENT/SHOPS/SHOP-CUSTOMER-DASHBOARD-1 -->
        <!-- END: PAGE CONTENT -->
    </div>
</section>

</div>
<!-- BEGIN: CONTENT/SHOPS/SHOP-2-2 -->


@endsection
