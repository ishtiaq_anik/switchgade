@extends('layouts.master')

@section('title', 'Product Details')

@section('content')
<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->

<section>
    <div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordegreen1 c-bordegreen1-both">
        <div class="container">
            <div class="c-page-title c-pull-left">
                <h3 class="c-font-uppercase c-font-sbold">Product Details 2</h3>
                <h4 class="">Page Sub Title Goes Here</h4>
            </div>
            <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
                <li><a href="shop-product-details-2.html">Product Details 2</a></li>
                <li>/</li>
                <li class="c-state_active">SwitchGads Components</li>

            </ul>
        </div>
    </div>
</section>

<section>
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="c-content-title-1">
                <h3 class="c-center c-font-uppercase c-font-bold">Product Categories</h3>
                <div class="c-line-center"></div>
            </div>
            <!-- BEGIN: CONTENT/SHOPS/SHOP-2-7 -->
            <div class="c-bs-grid-small-space">

                <div class="row">
                    <div class="col-md-3 col-sm-6 c-margin-b-20">
                        <div class="c-content-product-2 c-bg-white c-border">
                            <div class="c-content-overlay">
                                <div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                    </div>
                                </div>
                                <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(assets/img/content/shop2/100.jpg);"></div>
                            </div>
                            <div class="c-info">
                                <p class="c-title c-font-16 c-font-slim">Smartphone &amp; Handset</p>
                                <p class="c-price c-font-14 c-font-slim">$548 &nbsp;
                                    <span class="c-font-14 c-font-line-through c-font-red">$600</span>
                                </p>
                            </div>
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group c-border-top" role="group">
                                    <a href="shop-product-wishlist.html" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Details</a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 c-margin-b-20">
                        <div class="c-content-product-2 c-bg-white c-border">
                            <div class="c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                    </div>
                                </div>
                                <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(assets/img/content/shop2/83.jpg);"></div>
                            </div>
                            <div class="c-info">
                                <p class="c-title c-font-16 c-font-slim">Camera Lens</p>
                                <p class="c-price c-font-14 c-font-slim">$548 &nbsp;
                                    <span class="c-font-14 c-font-line-through c-font-red">$600</span>
                                </p>
                            </div>
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group c-border-top" role="group">
                                    <a href="shop-product-wishlist.html" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Details</a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 c-margin-b-20">
                        <div class="c-content-product-2 c-bg-white c-border">
                            <div class="c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                    </div>
                                </div>
                                <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(assets/img/content/shop2/83.jpg);"></div>
                            </div>
                            <div class="c-info">
                                <p class="c-title c-font-16 c-font-slim">Camera Lens</p>
                                <p class="c-price c-font-14 c-font-slim">$548 &nbsp;
                                    <span class="c-font-14 c-font-line-through c-font-red">$600</span>
                                </p>
                            </div>
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group c-border-top" role="group">
                                    <a href="shop-product-wishlist.html" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Details</a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 c-margin-b-20">
                        <div class="c-content-product-2 c-bg-white c-border">
                            <div class="c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                    </div>
                                </div>
                                <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(assets/img/content/shop2/83.jpg);"></div>
                            </div>
                            <div class="c-info">
                                <p class="c-title c-font-16 c-font-slim">Camera Lens</p>
                                <p class="c-price c-font-14 c-font-slim">$548 &nbsp;
                                    <span class="c-font-14 c-font-line-through c-font-red">$600</span>
                                </p>
                            </div>
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group c-border-top" role="group">
                                    <a href="shop-product-wishlist.html" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Details</a>
                                </div>

                            </div>
                        </div>
                    </div>

                </div><!-- END: CONTENT/SHOPS/SHOP-2-7 -->
                <div class="row">
                    <div class="col-md-3 col-sm-6 c-margin-b-20">
                        <div class="c-content-product-2 c-bg-white c-border">
                            <div class="c-content-overlay">
                                <div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                    </div>
                                </div>
                                <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(assets/img/content/shop2/100.jpg);"></div>
                            </div>
                            <div class="c-info">
                                <p class="c-title c-font-16 c-font-slim">Smartphone &amp; Handset</p>
                                <p class="c-price c-font-14 c-font-slim">$548 &nbsp;
                                    <span class="c-font-14 c-font-line-through c-font-red">$600</span>
                                </p>
                            </div>
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group c-border-top" role="group">
                                    <a href="shop-product-wishlist.html" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Details</a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 c-margin-b-20">
                        <div class="c-content-product-2 c-bg-white c-border">
                            <div class="c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                    </div>
                                </div>
                                <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(assets/img/content/shop2/83.jpg);"></div>
                            </div>
                            <div class="c-info">
                                <p class="c-title c-font-16 c-font-slim">Camera Lens</p>
                                <p class="c-price c-font-14 c-font-slim">$548 &nbsp;
                                    <span class="c-font-14 c-font-line-through c-font-red">$600</span>
                                </p>
                            </div>
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group c-border-top" role="group">
                                    <a href="shop-product-wishlist.html" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Details</a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 c-margin-b-20">
                        <div class="c-content-product-2 c-bg-white c-border">
                            <div class="c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                    </div>
                                </div>
                                <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(assets/img/content/shop2/83.jpg);"></div>
                            </div>
                            <div class="c-info">
                                <p class="c-title c-font-16 c-font-slim">Camera Lens</p>
                                <p class="c-price c-font-14 c-font-slim">$548 &nbsp;
                                    <span class="c-font-14 c-font-line-through c-font-red">$600</span>
                                </p>
                            </div>
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group c-border-top" role="group">
                                    <a href="shop-product-wishlist.html" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Details</a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 c-margin-b-20">
                        <div class="c-content-product-2 c-bg-white c-border">
                            <div class="c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                    </div>
                                </div>
                                <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(assets/img/content/shop2/83.jpg);"></div>
                            </div>
                            <div class="c-info">
                                <p class="c-title c-font-16 c-font-slim">Camera Lens</p>
                                <p class="c-price c-font-14 c-font-slim">$548 &nbsp;
                                    <span class="c-font-14 c-font-line-through c-font-red">$600</span>
                                </p>
                            </div>
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group c-border-top" role="group">
                                    <a href="shop-product-wishlist.html" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Details</a>
                                </div>

                            </div>
                        </div>
                    </div>

                </div><!-- END: CONTENT/SHOPS/SHOP-2-7 -->
                <div class="row">
                    <div class="col-md-3 col-sm-6 c-margin-b-20">
                        <div class="c-content-product-2 c-bg-white c-border">
                            <div class="c-content-overlay">
                                <div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                    </div>
                                </div>
                                <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(assets/img/content/shop2/100.jpg);"></div>
                            </div>
                            <div class="c-info">
                                <p class="c-title c-font-16 c-font-slim">Smartphone &amp; Handset</p>
                                <p class="c-price c-font-14 c-font-slim">$548 &nbsp;
                                    <span class="c-font-14 c-font-line-through c-font-red">$600</span>
                                </p>
                            </div>
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group c-border-top" role="group">
                                    <a href="shop-product-wishlist.html" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Details</a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 c-margin-b-20">
                        <div class="c-content-product-2 c-bg-white c-border">
                            <div class="c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                    </div>
                                </div>
                                <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(assets/img/content/shop2/83.jpg);"></div>
                            </div>
                            <div class="c-info">
                                <p class="c-title c-font-16 c-font-slim">Camera Lens</p>
                                <p class="c-price c-font-14 c-font-slim">$548 &nbsp;
                                    <span class="c-font-14 c-font-line-through c-font-red">$600</span>
                                </p>
                            </div>
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group c-border-top" role="group">
                                    <a href="shop-product-wishlist.html" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Details</a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 c-margin-b-20">
                        <div class="c-content-product-2 c-bg-white c-border">
                            <div class="c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                    </div>
                                </div>
                                <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(assets/img/content/shop2/83.jpg);"></div>
                            </div>
                            <div class="c-info">
                                <p class="c-title c-font-16 c-font-slim">Camera Lens</p>
                                <p class="c-price c-font-14 c-font-slim">$548 &nbsp;
                                    <span class="c-font-14 c-font-line-through c-font-red">$600</span>
                                </p>
                            </div>
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group c-border-top" role="group">
                                    <a href="shop-product-wishlist.html" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Details</a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 c-margin-b-20">
                        <div class="c-content-product-2 c-bg-white c-border">
                            <div class="c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                    </div>
                                </div>
                                <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(assets/img/content/shop2/83.jpg);"></div>
                            </div>
                            <div class="c-info">
                                <p class="c-title c-font-16 c-font-slim">Camera Lens</p>
                                <p class="c-price c-font-14 c-font-slim">$548 &nbsp;
                                    <span class="c-font-14 c-font-line-through c-font-red">$600</span>
                                </p>
                            </div>
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group c-border-top" role="group">
                                    <a href="shop-product-wishlist.html" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Details</a>
                                </div>

                            </div>
                        </div>
                    </div>

                </div><!-- END: CONTENT/SHOPS/SHOP-2-7 -->
            </div>
        </div>
    </div>
</section>

<!-- BEGIN: CONTENT/SHOPS/SHOP-2-2 -->


@endsection
