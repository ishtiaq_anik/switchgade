<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

/*******************************************************************
======================= Frontend ====================================
*****************************************************************/

Route::get('/home', 'PublicController@home')->name('home');
Route::get('/product-list', 'PublicController@productList')->name('product_list');
Route::get('/product-details', 'PublicController@productDetails')->name('product_details');
Route::get('/product-categories', 'PublicController@productCategories')->name('product_categories');
Route::get('/user-dashboard', 'PublicController@userDashboard')->name('user_dashboard');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

/*******************************************************************
======================= Backend ====================================
*****************************************************************/


Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
